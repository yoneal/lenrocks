// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra')
const select = require ('puppeteer-select');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

puppeteer.launch({ headless: true}).then(async browser => {
    const context = await browser.createIncognitoBrowserContext();
    const page = await context.newPage()
    await page.goto('https://padlet.com/PayMayaRocks/OOTD')
    await page.waitForTimeout(5000)
    // await page.screenshot({ path: 'testresult.png', fullPage: true })

    const element = await select(page).getElement('div[id=wish-1953119709] i.oricon[data-v-72a1af94]:contains(heart_outline)');
    await element.click()
    await page.waitForTimeout(1000);


    await browser.close()
    console.log(`All done. ✨`)
})
